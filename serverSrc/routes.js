const siteMeta = require('./site-meta.js');
const projects = require('./projects.js');

const extend  = require('extend');
const path = require('path');

const nodemailer  = require('nodemailer');
const transporter = nodemailer.createTransport(
  'smtps://noreply.matport%40gmail.com:' + process.env.GMAIL_KEY + '@smtp.gmail.com'
);

module.exports = function (app) {
  app.get('/', function (req, res) {
    const viewData = extend(
      true,
      siteMeta(),
      {
        projects: projects.getMetaArray()
      }
    );

    res.render('index', viewData);
  });

  app.get('/contact', function (req, res) {
    res.render('contact', siteMeta());
  });

  app.get('/editor', function (req, res) {
    res.render('editor', siteMeta());
  });

  app.get('/email-portrait/:version', function (req, res) {
    res.sendFile(
      path.resolve(
        __dirname +
        '/../dist/images/email/portrait_' +
        req.params.version + '.jpg'
      )
    );
  });

  app.get('/:projectID', function (req, res) {
    const project = projects.get(req.params.projectID);
    if (project) {
      const viewData = extend(
        true,
        siteMeta(),
        {
          project: project,
          headerData: {
            title: `${project.name} - Lukas von Mateffy`,
            content: `${project.excerpt} - ${project.description}`
          }
        }
      );

      res.render('project', viewData);
    } else {
      res.sendStatus(404);
    }
  });

  app.post('/contact', function (req, res) {
    if (req.body && req.body.name && req.body.email && req.body.message) {
      const mailOptions = {
        from: '"Portfolio Contact Service" <noreply.matport@gmail.com>', // sender address
        to: 'lukas.mateffy@gmail.com', // list of receivers
        subject: '📯 Contact Request', // Subject line
        text: `
          New contact request from portfolio form. \n\n
          "${req.body.name}" <${req.body.email}>\n\n
          ${req.body.message}
        `
      };

      transporter.sendMail(mailOptions, function(error, info){
        if(error){
          res.sendStatus(500);
          console.log(error);
        }
        console.log('Message sent: ' + info.response);
        res.sendStatus(200);
      });
    }
  });
};
