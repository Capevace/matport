const fs = require('fs');
const path = require('path');
const showdown = require('showdown');
const converter = new showdown.Converter();

function projectPath(project) {
  let dir = __dirname + '/../dist/projects';

  if (project) {
    dir += '/' + project;
  }

  return path.resolve(dir);
}

function exists(name) {
  try {
    const items = fs.readdirSync(projectPath());
    return items.contains(name);
  } catch (e) {
    console.log('Error fetching projects:', e);
    return false;
  }
}

function getMetaArray() {
  let projects = [];

  try {
    // Read all items of project folder
    const items = fs.readdirSync(projectPath());

    for (let project of items) {
      // Make path for each project

      try {
        const projectMeta = JSON.parse(
          fs.readFileSync(`${projectPath(project)}/meta.json`, 'utf8')
        );

        // Add id to meta
        projectMeta.id = project;

        // Add Project meta to list
        projects.push(projectMeta);
      } catch (e) {
        console.error('Error while reading meta:', e);
      }
    }
  } catch (e) {
    console.error('Error reading directory:', e);
  } finally {
    return projects
      .sort((a, b) => { // Sort projects by index (0 = first, x = last)
        const aIndex = a.index || 0;
        const bIndex = b.index || 0;

        if (aIndex < bIndex) {
          return -1;
        } else if (aIndex > bIndex) {
          return 1;
        }

        return 0;
      });
  }
}

function get(name) {
  try {
    // Load meta data and parse json
    const project = JSON.parse(fs.readFileSync(`${projectPath(name)}/meta.json`, 'utf8'));

    project.id = name;

    // Load content markdown and add it to data
    const markdownContent = fs.readFileSync(`${projectPath(name)}/content.md`, 'utf8');
    const htmlContent = converter.makeHtml(markdownContent);

    project.content = htmlContent;

    return project;
  } catch (e) {
    console.log('Error loading project:', e);

    return null;
  }
}

module.exports.exists = exists;
module.exports.getMetaArray = getMetaArray;
module.exports.get = get;


// module.exports = {
//   'disco78': {
//     name: 'disco78.com',
//     tags: ['web'],
//     excerpt: 'Website Development with Social Media Integration',
//     content: '## Design\nThe design was the simplest part bla bla...\n\n## CMS\nThe website had to be easily modifieably due to the oftenly changing content.\n\n## Stuff here\nSome stuff'
//   },
//   'dj54': {
//     name: 'dj54.de',
//     tags: ['web'],
//     excerpt: 'Website Development with Social Media Integration',
//     slides: [
//       {
//         file: 'slide2.png',
//         alt: 'Alternate site on an iMac'
//       },
//       {
//         file: 'slide1.png',
//         alt: 'Website on an iMac'
//       }
//     ],
//     content: '## Design\nThe design was the simplest part bla bla...\n\n## CMS\nThe website had to be easily modifieably due to the oftenly changing content.\n\n## Stuff here\nSome stuff'
//   },
//   'strandsalon': {
//     name: 'Strandsalon iOS App',
//     tags: ['app', 'social'],
//     excerpt: 'iOS App Development for Beach Club',
//     slides: [
//       {
//         file: 'slide2.png',
//         alt: 'App Menu'
//       },
//       {
//         file: 'slide1.png',
//         alt: 'Monthly Flyer Screen'
//       }
//     ],
//     content: '## Design\nThe design had to be put together to correspond with the website bla.\n\n## Execution\nThe events had to be fetched from facebook which proved to be a challenge...\n\n## Usability\nUsers were notified with newest push news...'
//   },
//   'djmadmat': {
//     name: 'djmadmat.com - Relaunch',
//     tags: ['web'],
//     excerpt: 'Website Development with Social Media Integration',
//     slides: [
//       {
//         file: 'slide2.png',
//         alt: 'Alternate site on an iMac'
//       },
//       {
//         file: 'slide1.png',
//         alt: 'Website on an iMac'
//       }
//     ],
//     content: '## Design\nThe design was the simplest part bla bla...\n\n## CMS\nThe website had to be easily modifieably due to the oftenly changing content.\n\n## Stuff here\nSome stuff'
//   },
//   'djmadmat2014': {
//     name: 'djmadmat.com - 2014',
//     tags: ['web'],
//     excerpt: 'Website Development with Social Media Integration',
//     slides: [
//       {
//         file: 'slide2.png',
//         alt: 'Alternate site on an iMac'
//       },
//       {
//         file: 'slide1.png',
//         alt: 'Website on an iMac'
//       }
//     ],
//     content: '## Design\nThe design was the simplest part bla bla...\n\n## CMS\nThe website had to be easily modifieably due to the oftenly changing content.\n\n## Stuff here\nSome stuff'
//   },
//   'element-control': {
//     name: 'ElementControl',
//     tags: ['app'],
//     excerpt: 'Game Jam Development',
//     content: '## No power converters please....'
//   }
//   // ,
//   // 'dr-herrmann': {
//   //   name: 'Dr. Herrmann',
//   //   tags: ['web'],
//   //   excerpt: 'Development of Website for a Doctors Office',
//   //   content: '## No power converters please....'
//   // }
// };
