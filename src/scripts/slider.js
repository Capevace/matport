var slides = document.querySelectorAll('.slides .slide');
var currentSlide = 0;
var slideInterval = setInterval(nextSlide, 4000);

function init() {
  window.addEventListener('resize', resizeSlider);

  document
    .querySelector('.slider-button.left')
    .addEventListener('click', function() {
      clearInterval(slideInterval);
      slideInterval = setInterval(nextSlide, 4000);
      previousSlide();
    });

  document
      .querySelector('.slider-button.right')
      .addEventListener('click', function() {
        clearInterval(slideInterval);
        slideInterval = setInterval(nextSlide, 4000);
        nextSlide();
      });

  resizeSlider();
}

function resizeSlider() {
  var slider = document.querySelector('.slides');
  slider.style.height = ((slider.offsetWidth < 680) ? slider.offsetWidth * 0.6176470588 : 420) + 'px';
}

function previousSlide() {
  slides[currentSlide].className = 'slide';

  currentSlide -= 1;

  if (currentSlide < 0) {
    currentSlide = slides.length - 1;
  }

  slides[currentSlide].className = 'slide showing';
}

function nextSlide(){
  slides[currentSlide].className = 'slide';

  currentSlide += 1;

  if (currentSlide >= slides.length) {
    currentSlide = 0;
  }

  slides[currentSlide].className = 'slide showing';
}

module.exports = init;
