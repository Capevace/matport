(function() {
  require('./html5');
  require('string.prototype.startswith');
  var helpers = require('./helpers').helpers;

  helpers.ready(function() {
    if (document.querySelector('.slides'))
      require('./slider.js')();

    var shouldFadeOnUnload = true;

    function resetUnloadFade () {
      setTimeout(function () {
        shouldFadeOnUnload = true;
      }, 500);
    }

    function fade () {
      helpers.addClass(document.querySelector('.page'), 'fadeOut');
    }

    // Make whole site fade when changing url
    window.onbeforeunload = function(){
      if (shouldFadeOnUnload)
        fade();
    };

    // Make all links fade when clicked
    for (var link of document.getElementsByTagName('a')) {
      link.addEventListener('click', function(e) {
        // Prevent redirection of link
        var href = this.getAttribute('href').toString();
        if (!href.startsWith('#') && !href.startsWith('mailto:')) {
          e.preventDefault();

          shouldFadeOnUnload = false;
          resetUnloadFade();
          fade();
          setTimeout(function () {
            window.location.href = href;
          }, 300);
        } else if (href.startsWith('mailto:')) {
          shouldFadeOnUnload = false;
          resetUnloadFade();
        }
      });
    }

    // Show all Objects which require js
    var objs = document.querySelectorAll('.js-required');
    for (var el of objs) {
      helpers.removeClass(el, 'js-required');
    }

    // Contact form
    var contactForm = document.querySelector('form#contact');
    if (contactForm) {
      contactForm.addEventListener('submit', function(e) {
        e.preventDefault();

        if (this.querySelector('#fake-name-field').value != '') {
          console.error('I guess you\'re a bot then...');
          return;
        }

        var $nameField = this.querySelector('#name-field');
        var $emailField = this.querySelector('#email-field');
        var $messageField = this.querySelector('#message-field');
        var $submitButton = this.querySelector('#submit-button');
        var $errorElement = this.querySelector('#form-error-field');

        $nameField.disabled = $emailField.disabled = $messageField.disabled = true;
        helpers.addClass($submitButton, 'loading');

        var error = function (message) {
          helpers.addClass($errorElement, 'text');
          helpers.addClass($errorElement, 'error');
          $errorElement.innerHTML = message;
          $errorElement = null;
        };

        var success = function (message) {
          helpers.addClass($errorElement, 'text');
          helpers.addClass($errorElement, 'secondary');
          $errorElement.innerHTML = message;
          $errorElement = null;
        };

        var stopLoading = function (successState) {
          $nameField.disabled = $emailField.disabled = $messageField.disabled = false;
          helpers.removeClass($submitButton, 'loading');

          if (successState) {
            $nameField.value = '';
            $emailField.value = '';
            $messageField.value = '';
            success('Message successfully sent! I will get back to you as soon as possible.');
            error = null;
          } else {
            error('An error occurred while sending your message... :( <br>Please try contacting me by <a href="mailto:' + document.emailAddress + '">email</a>.');
            success = null;
          }

        };

        setTimeout(stopLoading, 5000);

        helpers.post(
          '/contact',
          {
            name: $nameField.value,
            email: $emailField.value,
            message: $messageField.value
          },
          function() {
            // Success
            stopLoading(true);
          },
          function(e) {
            // Error
            console.log('Error occured at form submitting:', e);
            stopLoading(false);

          }
        );
      });
    }
  });
})();
