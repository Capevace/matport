const argv = require('yargs').argv;

process.env.NODE_ENV = 'development';

if (argv.e) {
  process.env.NODE_ENV = argv.e;
}

console.log('Running in profile:', process.env.NODE_ENV);

require('./server');
