module.exports = () => ({
  headerData: {
    title: 'Lukas von Mateffy\'s Portfolio',
    content: 'I am Lukas!!',
    keywords: 'keywords here pls'
  },
  otherData: {
    email: 'lukas@smoolabs.com',
    url: 'https://matport.herokuapps.com',
    description: 'My name is Lukas von Mateffy! ...'
  }
});
