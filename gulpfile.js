const gulp    = require('gulp'),
  $           = require('gulp-load-plugins')(),
  through2    = require('through2'),
  browserify  = require('browserify'),
  del         = require('del');

gulp.task('compass', function() {
  return gulp.src('./src/stylesheets/main.scss')
    .pipe($.plumber())
    .pipe($.compass({
      css: 'dist/stylesheets',
      sass: 'src/stylesheets'
    }))
    .on('error', function(err) {
      console.error(err);
    })
    .pipe($.cleanCss({compatibility: 'ie8'}))
    .pipe(gulp.dest('dist/stylesheets'));
});

gulp.task('js', function() {
  return gulp.src('src/scripts/main.js')
    .pipe($.plumber())
    .pipe(through2.obj(function (file, enc, next) {
      browserify(file.path, { debug: true })
        .transform(require('babelify'))
        .transform(require('debowerify'))
        .bundle(function (err, res) {
          if (err) { return next(err); }
          file.contents = res;
          next(null, file);
        });
    }))
      .on('error', function (error) {
        console.log(error.stack);
        this.emit('end');
      })
  .pipe($.uglify())
  .pipe($.rename('app.js'))
  .pipe(gulp.dest('dist/scripts/'));
});

gulp.task('clean', function(cb) {
  del('./dist', cb);
});

gulp.task('images', function() {
  return gulp.src('./src/images/**/*')
    .pipe(gulp.dest('./dist/images'));
});

gulp.task('images-production', function() {
  return gulp.src('./src/images/**/*')
    .pipe($.imagemin({
      progressive: true
    }))
    .pipe(gulp.dest('./dist/images'));
});

gulp.task('views', function() {
  return gulp.src('src/views/**/*.mustache')
    .pipe($.plumber())
    .pipe(gulp.dest('dist/views'));
});

gulp.task('fonts', function () {
  return gulp.src('src/fonts/**/*')
    .pipe($.plumber())
    .pipe(gulp.dest('dist/fonts/'));
});

gulp.task('documents', function () {
  return gulp.src('src/documents/**/*')
    .pipe($.plumber())
    .pipe(gulp.dest('dist/documents/'));
});

gulp.task('projects', function () {
  return gulp.src('src/projects/**/*')
    .pipe($.plumber())
    .pipe(gulp.dest('dist/projects/'));
});

gulp.task('build', ['compass', 'js', 'fonts', 'views', 'documents', 'projects']);
gulp.task('debug', ['build', 'images']);
gulp.task('production', ['build', 'images-production']);

gulp.task('serve', ['debug'], function () {
  gulp.watch('src/stylesheets/**/*.{scss,sass}',['compass']);
  gulp.watch('src/scripts/**/*.js',['js']);
  gulp.watch('src/images/**/*',['images']);
  gulp.watch('src/fonts/*',['fonts']);
  gulp.watch('src/views/*',['views']);
  gulp.watch('src/documents/**/*',['documents']);
  gulp.watch('src/projects/**/*',['projects']);
});

gulp.task('default', ['serve']);
