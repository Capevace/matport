var helpers = require('./helpers').helpers;
var portfolio = require('./portfolio');

module.exports = {
  body: null,
  popup: null,

  show: function (id) {
    if (!(id in portfolio)) {
      return;
    }

    if (!this.body) {
      this.body = document.querySelector('body');
    }

    if (!this.popup) {
      this.popup = document.querySelector('.popup');
    }

    helpers.addClass(this.popup, 'show');
    helpers.addClass(this.body, 'no-scroll');

    var item = portfolio[id];
    alert(JSON.stringify(item));
  },

  hide: function () {
    helpers.removeClass(this.popup, 'show');
    helpers.removeClass(this.body, 'no-scroll');
  }
};
