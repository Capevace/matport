const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mustacheExpress = require('mustache-express');
const routes = require('./routes');

// Require dotenv so enviroments are loaded on local test machine as well
require('dotenv')
  .config({
    path: '.serverEnv',
    silent: true
  });

// Setup view engine
app.engine('mustache', mustacheExpress());
app.set('view engine', 'mustache');
app.set('views', __dirname + '/../dist/views');

// Set ports
if (process.env.OPENSHIFT_NODEJS_PORT) {
  console.log('OpenShift port:', process.env.OPENSHIFT_NODEJS_PORT);
}

if (process.env.PORT) {
  console.log('Heroku port:', process.env.PORT);
}

app.set('ipaddress', (process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1'));
app.set('port', (process.env.OPENSHIFT_NODEJS_PORT || process.env.PORT || 5000));

// Serve static files from /dist if requested
app.use(express.static(__dirname + '/../dist'));

// Use Http Body parsers
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Setup Application Routes
routes(app);

const listen = function () {
  console.log('Server listening on port: ' + app.get('port'));
};

if (process.env.OPENSHIFT_NODEJS_PORT) {
  app.listen(app.get('port'), app.get('ipaddress'), listen);
} else {
  app.listen(app.get('port'), listen);
}
