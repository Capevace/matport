exports.helpers = {
  /* Methods */
  ready: function(fn) {
    if (document.readyState != 'loading'){
      fn();
    } else {
      document.addEventListener('DOMContentLoaded', fn);
    }
  },

  addClass: function(el, className) {
    if (el.classList)
      el.classList.add(className);
    else
      el.className += ' ' + className;
  },

  removeClass: function(el, className) {
    if (el.classList)
      el.classList.remove(className);
    else
      el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
  },

  post: function (url, data, callback, errorCallback) {
    var request = new XMLHttpRequest();

    request.addEventListener('load', function () {
      if(request.readyState == 4 && request.status == 200) {
        callback(request.responseText);
      } else {
        errorCallback(request.status);
      }
    });
    request.open('POST', url, true);
    request.setRequestHeader('Content-Type', 'application/json charset=UTF-8');
    request.send(JSON.stringify(data));
  }
};
